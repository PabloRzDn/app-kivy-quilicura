import geocoder

class GeoLocalizacion:
    def __init__(self):
        self.google_api="AIzaSyC7eo0mIhVRDcnRDluih7auLaHqC0DV7zw"
        self.localizacion=geocoder.ip("me")
        self.url="http://www.google.com/maps/search/?api=1&"

    def obtener_localizacion(self):
        self.lat=self.localizacion.latlng[0]
        self.long=self.localizacion.latlng[1]
        
        self.base_url="{}{}|{}".format(self.url,self.lat,self.long)
        return self.base_url