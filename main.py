######### Dependencias y módulos externos #########
from os import pathconf_names
import sys
import sqlite3

from typing import Text
from kivy.app import App

from kivy.uix.button import Button 
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput

from kivy.uix.widget import Widget
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelHeader
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivymd.app import MDApp
from kivy.config import Config
########## ########## ########## ########## ##########


########## Dependencias y módulos internos ##########

from modules.BotTelegram import TelegramBot
from modules.db import BaseDeDatos
from modules.geolocalizacion import GeoLocalizacion

########## ########## ########## ########## ##########



Builder.load_file("main.kv")

#Tamaño de la ventana 
#Window.size=(412,846)
Config.set("graphics","resizable",True)
#se definen las diferentes ventanas del proyecto


class FormularioRegistro(Screen):
    def __init__(self, **kwargs):
        super(FormularioRegistro,self).__init__(**kwargs)


    def submit_usuaria(self):
        nombresUsuaria=self.ids.nombres_usuaria.text,
        apellidosUsuaria=self.ids.apellidos_usuaria.text,
        direccionUsuaria=self.ids.direccion_usuaria.text,
        numeroUsuaria=self.ids.numero_usuaria.text,
        departamentoUsuaria=self.ids.departamento_usuaria.text,
        emailUsuaria=self.ids.email_usuaria.text,
        telefonoUsuaria=self.ids.telefono_usuaria.text
        
        usuaria=BaseDeDatos()
        usuaria.crear_usuaria(nombresUsuaria[0],
                                apellidosUsuaria[0],
                                direccionUsuaria[0],
                                numeroUsuaria[0],
                                departamentoUsuaria[0],
                                emailUsuaria[0],
                                telefonoUsuaria[0])


    pass

class FormularioRegistroContacto(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    
    def submit_contacto(self):
        nombresContacto=self.ids.nombres_contacto.text,
        apellidosContacto=self.ids.apellidos_contacto.text,
        emailContacto=self.ids.email_contacto.text,
        telefonoContacto=self.ids.telefono_contacto.text

        contacto=BaseDeDatos()
        contacto.crear_contacto(
                                nombresContacto[0],
                                apellidosContacto[0],
                                emailContacto[0],
                                telefonoContacto[0])

    pass

class PantallaIndex(Screen):
    def __init__(self, **kwargs):
        super(PantallaIndex,self).__init__(**kwargs)

    def enviar_mensaje_a_bot(self):
        datos_usuaria=BaseDeDatos()
        datos_usuaria=datos_usuaria.seleccionar_primera_fila()
        datos_usuaria=datos_usuaria[1][0]

        mensaje=TelegramBot()
        mensaje.enviar_mensaje_a_canal(datos_usuaria)
    
        pass
    pass

    def enviar_localizacion_a_bot(self):
        datos_usuaria=BaseDeDatos()
        datos_usuaria=datos_usuaria.seleccionar_primera_fila()
        datos_usuaria=datos_usuaria[1][0]

        localizacion_mapa=GeoLocalizacion()
        localizacion_mapa=localizacion_mapa.obtener_localizacion()

        mensaje=TelegramBot()
        mensaje=mensaje.enviar_localizacion_a_canal(datos_usuaria,localizacion_mapa)

class PantallaInformacion(Screen):
    pass

class MenuUsuario(Screen):
    pass

class MensajeTelegram(Screen):
    pass

class EnviarUbicacion(Screen):
    pass

class BotTelegram(Screen):
    pass



class AdministradorPantallas(ScreenManager):
    pass





class MainApp(MDApp):
    def build(self):

        #Base de datos
        db=BaseDeDatos()
        db.crear_tablas_si_no_existen()
        
        seleccion=BaseDeDatos()
        seleccion=seleccion.seleccionar_primera_fila()
        print(seleccion, file=sys.stderr)
        #Creación de pantallas
        self.sm=ScreenManager()

        if seleccion==0:
            self.sm.add_widget(FormularioRegistro(name="FormularioRegistro"))
            self.sm.add_widget(FormularioRegistroContacto(name="FormularioRegistroContacto"))
            self.sm.add_widget(PantallaIndex(name="pantalla_index"))
            self.sm.add_widget(PantallaInformacion(name="pantalla_informacion"))

            self.sm.add_widget(MensajeTelegram(name="MensajeTelegram"))
            self.sm.add_widget(EnviarUbicacion(name="EnviarUbicacion"))
            self.sm.add_widget(BotTelegram(name="BotTelegram"))

        else:
            self.sm.add_widget(PantallaIndex(name="pantalla_index"))
            self.sm.add_widget(PantallaInformacion(name="pantalla_informacion"))
            
            self.sm.add_widget(MensajeTelegram(name="MensajeTelegram"))
            self.sm.add_widget(EnviarUbicacion(name="EnviarUbicacion"))
            self.sm.add_widget(BotTelegram(name="BotTelegram"))



        
        

        return self.sm
    
    def change_screen(self,pantalla):
        self.sm.current=pantalla
    

if __name__=="__main__":
    MainApp().run()