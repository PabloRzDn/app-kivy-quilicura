

import datetime
import requests, sys
from kivy.network.urlrequest import UrlRequest

class TelegramBot:
    def __init__(self):
       
        #telegram data
        self.url_api=["http://api.telegram.org/bot","/sendMessage?chat_id=","&text="]
        self.bot_token="2124783491:AAGqgwJX2tIrB1_je4IH9OJYlQh0jw4XYw8"
        self.chat_id="-1001537974149"

        #user data

        


    def enviar_mensaje_a_canal(self,data_usuaria):
        self.usuaria="{} {}".format(data_usuaria[1],data_usuaria[2])
        self.direccion="{} {} Dpto. {}".format(data_usuaria[3],data_usuaria[4],data_usuaria[5])
        self.marca_tiempo="{} {}".format(str(datetime.date.today().strftime("%d-%m-%Y")),str(datetime.datetime.now().strftime("%H:%M:%S")))
        self.mensaje='La siguiente usuaria ha enviado un mensaje de alerta urgente: {} / {} / {}'.format(self.usuaria,self.direccion,self.marca_tiempo)


        self.base_url='{}{}{}{}{}"{}"'.format(self.url_api[0],self.bot_token,self.url_api[1],self.chat_id,self.url_api[2],self.mensaje)
      
       
        try:
            r=requests.get(self.base_url)
            print("URL: ",self.base_url, file=sys.stderr)
            print("status: ",r.status_code, file=sys.stderr)
            print("[+] Mensaje enviado satisfactoriamente.", file=sys.stderr)
            return self.mensaje
        except requests.exceptions.RequestException as e:
            print("[!] Error de conexión:",e)
            

    def enviar_localizacion_a_canal(self,data_usuaria,url):

        self.usuaria="{} {}".format(data_usuaria[1],data_usuaria[2])
        self.url=url
        self.marca_tiempo="{} {}".format(str(datetime.date.today().strftime("%d-%m-%Y")),str(datetime.datetime.now().strftime("%H:%M:%S")))

        self.mensaje="La usuaria {} ha enviado su localización {}".format(self.usuaria,self.url)
        self.base_url='{}{}{}{}{}"{}"'.format(self.url_api[0],self.bot_token,self.url_api[1],self.chat_id,self.url_api[2],self.mensaje)
        try:
            r=requests.get(self.base_url)
            print("URL: ",self.base_url, file=sys.stderr)
            print("status: ",r.status_code, file=sys.stderr)
            print("[+] Mensaje enviado satisfactoriamente.", file=sys.stderr)
            return self.mensaje
        except requests.exceptions.RequestException as e:
            print("[!] Error de conexión:",e)