import sqlite3

class BaseDeDatos:

    def crear_tablas_si_no_existen(self):

        self.query_usuaria="""
                    CREATE TABLE IF NOT EXISTS usuaria (
                        id INTEGER PRIMARY KEY AUTOINCREMENT, 
                        nombres TEXT,
                        apellidos TEXT ,
                        direccion TEXT ,
                        numero TEXT ,
                        departamento TEXT,
                        email TEXT ,
                        telefono TEXT 
                    );
        
                   """
        
        self.query_contacto="""
                    CREATE TABLE IF NOT EXISTS contacto (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        nombres TEXT ,
                        apellidos TEXT ,
                        telefono TEXT ,
                        email TEXT 

                    );
        
                   """
        self.conn=sqlite3.connect("usuaria.db")
        self.cursor=self.conn.cursor()
        self.cursor.execute(self.query_usuaria)
        self.cursor.execute(self.query_contacto)
        self.conn.commit()
        self.conn.close()
    
    def crear_usuaria(self,nombres,apellidos,direccion,numero,departamento,email,telefono):

        self.query_crear_usuaria="""
                                 INSERT INTO usuaria (nombres,apellidos,direccion,numero,departamento,email,telefono) VALUES (?,?,?,?,?,?,?)       
                                 
                                 """
        
        self.conn=sqlite3.connect("usuaria.db")
        self.cursor=self.conn.cursor()
        self.cursor.execute(self.query_crear_usuaria,
            (nombres,
            apellidos,
            direccion,
            numero,
            departamento,
            email,
            telefono)
        )
        self.conn.commit()
        self.conn.close()
        
    
    def crear_contacto(self,nombres,apellidos,email,telefono):
        self.query_crear_contacto="""
                                 INSERT INTO contacto (nombres,apellidos,email,telefono) VALUES (?,?,?,?)       
                                 
                                 """
        
        self.conn=sqlite3.connect("usuaria.db")
        self.cursor=self.conn.cursor()
        self.cursor.execute(self.query_crear_contacto,
            (nombres,
            apellidos,
            email,
            telefono))
        self.conn.commit()
        self.conn.close()
        pass


    def seleccionar_primera_fila(self):
        self.query_seleccion="""SELECT * FROM usuaria"""
        self.conn=sqlite3.connect("usuaria.db")
        self.cursor=self.conn.cursor()

        self.cursor.execute(self.query_seleccion)
        self.seleccion=self.cursor.fetchall()
        self.conn.commit()
        self.conn.close()
        
        if self.seleccion==[]:
            return 0
        else:
            return 1, self.seleccion
    

    def seleccionar_contacto(self):
        self.query_seleccion="""SELECT * FROM contacto"""
        self.conn=sqlite3.connect("usuaria.db")
        self.cursor=self.conn.cursor()

        self.cursor.execute(self.query_seleccion)
        self.seleccion=self.cursor.fetchall()
        self.conn.commit()
        self.conn.close()
        return self.seleccion